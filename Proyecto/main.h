/* Esta función optimiza un enlace para utilizarlo mejor en el programa.
E: Una dirección URL.
S: La misma, pero recortada.
R: El argumento de entrada debe ser un enlace. */
char recortar_link(char url[32]);

/* Esta función habilita un nodo nuevo basado en una dirección URL.
E: Un enlace web.
S: Un nodo.
R: El argumento de entrada debe ser un enlace. */
nodo *crear_nodo(char url[32]);

/* Esta función descarga los 500 enlaces de https://moz.com/top500.
E: N/A
S: Los 500 enlaces.
R: Se necesita conexión a internet. */
void descargar_urls();

/* Esta función inserta una dirección URL en el árbol de enlaces.
E: Un nodo que apunta a arbol.
S: N/A
R: El nodo debe ser una dirección URL. */
void insertar_url(nodo *arbol, char url[32]);

/* Esta función retorna un nodo particular que se desea hallar.
E: Una dirección URL.
S: Un nodo.
R: El argumento de entrada debe ser un enlace. */
nodo *buscar_nodo(char url[32]);

// Función auxiliar de 'borrar_url'.
void borrar_url_aux(nodo *arbol);

/* Esta función elimina una dirección URL en el árbol de enlaces.
E: Un nodo que apunta a arbol.
S: N/A
R: El nodo debe ser una dirección URL. */
void borrar_url(nodo *arbol);

/* Esta función permite referirse a un enlace por un alias.
E: Una dirección URL y un alias para el mismo.
S: La referencia por alias.
R: Los argumentos de entrada deben ser un enlace y un alias. */
void actualizar_alias(char url[32], char alias[32]);

/* Esta función imprime el menú principal.
E: N/A
S: Las distintas opciones del programa.
R: N/A */
void menu();

/* Función inicial que arranca el programa.
E: N/A
S: Menú principal.
R: N/A */
int main();