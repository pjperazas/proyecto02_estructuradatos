#include <stdio.h>
#include <string.h>
#include <stdlib.h>
//==========================================================================================================================
char url[32];
char alias[32];
void insertar_url();
void borrar_raiz();
char abrir_en_Chrome[64] = {"chrome "};
//==========================================================================================================================
typedef struct nodo {

  char url[32];
  char alias[32];
  struct nodo* padre;
  struct nodo* der;
  struct nodo* izq;

} nodo;
//==========================================================================================================================
char recortar_link(char url[32]) {

	int posicion_ultimo_char = strlen(url);

	posicion_ultimo_char--;
	url[posicion_ultimo_char] = '\000';
	posicion_ultimo_char--;
	url[posicion_ultimo_char] = '\000';

	for (int i = 0; i < posicion_ultimo_char + 1; i++) {
		url[i] = url[i + 1];
	}
	return url[32];
}
//==========================================================================================================================
nodo *arbol_links = NULL;
//==========================================================================================================================
nodo *crear_nodo(char url[32]) {
	nodo *nuevo_nodo;
	nuevo_nodo = malloc(sizeof(nodo));
	strcpy(nuevo_nodo->url, url);
	strcpy(nuevo_nodo->alias, "Sin Alias");
	nuevo_nodo->der = NULL;
	nuevo_nodo->izq = NULL;
	nuevo_nodo->padre = NULL;
	return nuevo_nodo;
}
//==========================================================================================================================

void descargar_urls() {
  system("wget -O './top500Domains.csv' https://moz.com/top-500/download/?table=top500Domains");
	system("column -s, -t < top500Domains.csv >> temp.txt"); //separa las columnas
	system("awk '{ print $2 }' temp.txt >> listaDeURLs.txt"); // adquiere las columnas
	system("rm temp.txt"); // elimina el archivo de texto temp
	FILE *links;
	links = fopen("links.txt", "r");

	char  url_array[32];

	while (!feof(links))   //feof = final de archivo
	{
		fgets(url_array, 32, links);
    if (strcmp(url_array, "\"Root\n") != 0)
		{
      url_array[32] = recortar_link(url_array);
			insertar_url(arbol_links, url_array);
		}

	}
	fclose(links);
}
//==========================================================================================================================
void insertar_url(nodo *arbol, char url[32]) {

	if (arbol_links == NULL){
		arbol_links = crear_nodo(url);
	} else {
		if (strcmp(arbol->url, url) == 1) {
			if (arbol->izq == NULL) {

				nodo *nuevo_nodo = crear_nodo(url);
				arbol->izq = nuevo_nodo;
				nuevo_nodo->padre = arbol;
			}
			insertar_url(arbol->izq, url);
		} else {
			if (arbol->der == NULL) {
				nodo *nuevo_nodo = crear_nodo(url);
				arbol->der = nuevo_nodo;
				nuevo_nodo->padre = arbol;
			}
			insertar_url(arbol->der, url);
		}
	}
}
//==========================================================================================================================
nodo *buscar_menor(nodo *arbol) {

  if (arbol->der->izq == NULL) {
    return arbol->der;
  } else {
    nodo *nodo_i = arbol->der;
    while (nodo_i->izq != NULL) {
      nodo_i = nodo_i->izq;
    }
    return nodo_i;
  }
}

//==========================================================================================================================
nodo *buscar_nodo(char url[32]) {
	if (arbol_links == NULL)	{
		return NULL;
	} else {

		nodo *nodo_actual = arbol_links;
		while (nodo_actual != NULL) {

			if (strcmp(url, nodo_actual->url) < 0) {
				printf("%s\n", nodo_actual->url);
				nodo_actual = nodo_actual->izq;
			}
			else if (strcmp(url, nodo_actual->url) > 0) {
				printf("%s\n", nodo_actual->url);
				nodo_actual = nodo_actual->der;
			} else {
				printf("Dirección URL encontrada.\n");
				return nodo_actual;
			}
		}
		return nodo_actual;
	}
}
//==========================================================================================================================
void borrar_url_aux(nodo *arbol) {
  arbol->izq = NULL;
  arbol->der = NULL;
  free(arbol);
}
//==========================================================================================================================================================================
void borrar_url(nodo *arbol) {
    if(arbol == arbol_links){
       borrar_raiz();
       return;
    } 
    else if (arbol->izq == NULL && arbol->der == NULL) {
        if (arbol->padre->der == arbol) {
          arbol->padre->der = NULL;
        } else {
          arbol->padre->izq = NULL;
        }
        borrar_url_aux(arbol);
        arbol->padre = NULL;
        return;
    }
    else if (arbol->der == NULL) {  // elimina si solo tiene un hijo izq
      if (arbol->padre->der == arbol) {
        arbol->padre->der = arbol->izq;
      } else {
        arbol->padre->izq = arbol->izq;
      }
      borrar_url_aux(arbol);
    } 
    else if (arbol->izq == NULL) {  // elimina si solo tiene un hijo der
      if (arbol->padre->der == arbol) {
        arbol->padre->der = arbol->der;
      } else {
        arbol->padre->izq = arbol->der;
      }
      borrar_url_aux(arbol);
    }
    else {  // elimina si  tiene dos hijos
      nodo *menor_derecha = buscar_menor(arbol_links);
      strcpy(arbol->url, menor_derecha->url);
      strcpy(arbol->alias, menor_derecha->alias);
      borrar_url(menor_derecha);
  }
}

//==========================================================================================================================
void actualizar_alias(char url[32], char alias[32]) {
	nodo *nuevo_alias_nodo = buscar_nodo(url);
	if (nuevo_alias_nodo == NULL) {
		printf("no existe tal url\n");
	} else {
		strcpy(nuevo_alias_nodo->alias, alias);
	}
}

//==========================================================================================================================

void borrar_raiz() {
	nodo *temp = arbol_links;
	if (arbol_links->izq == NULL && arbol_links->der == NULL) {
		borrar_url_aux(temp);
		arbol_links = NULL; 
	}
	else if (arbol_links->der == NULL) {
		arbol_links = arbol_links->izq;
		arbol_links->padre = NULL;
		borrar_url_aux(temp);
	}
	else if (arbol_links->izq == NULL) {
		arbol_links = arbol_links->der;
		arbol_links->padre = NULL;
		borrar_url_aux(temp);
	} else {
		nodo *menor_derecha = buscar_menor(arbol_links);
		arbol_links->url[32] = '\0';
		strcpy(arbol_links->url, menor_derecha->url);
		arbol_links->alias[32] = '\0';
		strcpy(arbol_links->alias, menor_derecha->alias);
		borrar_url(menor_derecha);
	}
}
void menu() {
  int opcion;
  while (1) {
      printf("\033[22;34m┌────────────────────────────────┐\n");
      printf("│\x1b[0m ************ \x1b[36mMENÚ\x1b[36m\x1b[0m ************ \033[22;34m│\n");
      printf("╞════════════════════════════════╡\n");
      printf("│ \x1b[36m1. Descargar URLs\033[22;34m              │\n");
      printf("├────────────────────────────────┤\n");
      printf("│ \x1b[36m2. Insertar URL\033[22;34m                │\n");
      printf("│ \x1b[36m3. Borrar URL\033[22;34m                  │\n");
      printf("│ \x1b[36m4. Actualizar Alias\033[22;34m            │\n");
      printf("│ \x1b[36m5. Abrir URL\033[22;34m                   │\n");
      printf("│ \x1b[36m6. Salir\033[22;34m                       │\n");
      printf("└────────────────────────────────┘\n\n");
      printf("\x1b[36mElija una opción y presione Enter: \x1b[0m");
      scanf("%d", &opcion);
      
      switch (opcion) {
	case 1:
    descargar_urls();
    printf("La descarga se realizó exitosamente.");
    break;
  
  case 2:
    printf("Escriba la dirección URL: ");
		scanf("%s", url);
		insertar_url(arbol_links, url);
    break;

  case 3:
    printf("Escriba  la dirección URL: ");
		scanf("%s", url);
    nodo *borrar = buscar_nodo(url);
		borrar_url(borrar);
    printf("Direccion eliminada\n");
    break;
  
  case 4:
    printf("Escriba la dirección URL: ");
		scanf("%s", url);
		printf("Digite el alias que desea agregar: ");
		scanf("%s", alias);
		actualizar_alias(url, alias);
    break;

  case 5:
    printf("Escriba la dirección por abrir: ");
		scanf("%s", url);
		nodo *nodo_por_abrir = buscar_nodo(url);

    if (nodo_por_abrir == NULL) {
		  printf("Dirección URL no válida.\n");
		  return;
	  }
	  char url[32];
	  strcpy(url, nodo_por_abrir->url);
  	strcat(abrir_en_Chrome, url);
  	system(abrir_en_Chrome);
    break;

  case 6:
    printf("Eliminando URLs...\n");
		system("rm listaDeURLs.txt");
		break;

  default:
    printf("Opción no válida. Digite un número del 1 al 5.");
    break;
	  }
  }
}
//==========================================================================================================================
int main() {
  menu();
  return 0;
}